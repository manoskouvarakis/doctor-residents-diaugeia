# diaugeia-doctor-residents

Given a specific doctor resident name, find newly published decisions that are published on "diaugeia".
 
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes

### Prerequisites

In order to run the application locally, you will only need [Node.js](https://nodejs.org/en/) installed on your computer. 

### Configuration

In configuration file (config.js) you can set:
- The full name in greek
- The interval (in minutes) of each search
- Enable/Disable Mail
- If mail is enabled, you should set user, pass and recipients addresses

### Installation

In order to run the project:

```
- Clone the repository in your pc
- Navigate to /diaugeia-doctor-residents
- Run "npm install"
```

### Execution

```
- Navigate to /diaugeia-doctor-residents/app
- Run "node index.js" 
```

### Built With

* [NodeJS](https://nodejs.org)
* [LokiJS](http://lokijs.org/) - In-memory NoSQL database
* [Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js


