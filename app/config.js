const config = {
	app: {
		minutesInterval: 1,
		baseUrl: 'https://diavgeia.gov.gr/opendata/search/advanced.json'		
	}, 
	query: {
		text: '',
		organizationUid: '',
		decisionType: ''
	},
	mail: {
		enable: false,
		user: '****',
		pass: '****',
		recepients: []
	}
};

module.exports = config;