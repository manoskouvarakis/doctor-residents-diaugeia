const mail    = require('./lib/mail');
const axios = require("axios");
const https = require('https');
const loki = require('lokijs');
const config = require('./config');

var app = {};

app.db = new loki('diaugeia.db', {
    autosave: true, 
    autosaveInterval: 4000
});

app.init = async function() {
	if (config.mail.enable) {
		mail.init();
	}
    
	await initializeDatabase(); 
    
	setInterval(app.execute, 1000 * 60 * config.app.minutesInterval);
};

async function initializeDatabase(options) {
  return new Promise((resolve, reject) => app.db.loadDatabase(options, err => err ? reject(err) : resolve(null)))
}

app.execute = async function() {
    let decisionsCollection = app.db.getCollection("decisions");
    if (decisionsCollection === null) {
        decisionsCollection = app.db.addCollection("decisions");
    }
    
    let diaugeiaResults = await app.getDiaugeiaResults();
    //console.log(diaugeiaResults.decisions);

    diaugeiaResults.decisions.forEach(function(decision) {
        let existingDecision = decisionsCollection.findOne({'ada': decision.ada});
        if (existingDecision == null) {
            console.log('New decision found with ada: ' + decision.ada);
            console.log(decision);
            decisionsCollection.insert(decision);
						
            if (config.mail.enable) {
            	console.log('Sending mail');
                mail.send(config.mail.recepients, 'A new decision uplodated for you', `New Diaugeia Decision on ${decision.documentUrl}`);
            }
        }
    });
};

app.buildQuery = function() {
    try {
        let queryString = '';
        if (config.query.text !== '') {
            if (queryString !== '') queryString += ' AND ';

            queryString += 'q:"' + config.query.text + '"';
        }
        if (config.query.organizationUid !== '') {
            if (queryString !== '') queryString += ' AND ';

            queryString += 'organizationUid:"' + config.query.organizationUid + '"';
        }
        if (config.query.decisionType !== '') {
            if (queryString !== '') queryString += ' AND ';

            queryString += 'decisionType:"' + config.query.decisionType + '"';
        }

        let encodedQuery = encodeURIComponent(queryString);

        return encodedQuery;
    } catch (error) {
        console.log(error);
    }
};

app.getDiaugeiaResults = async function() {
    try {
        console.log('Fetching ' + new Date(Date.now()).toString());

        const agent = new https.Agent({  
          rejectUnauthorized: false
        });

        let baseUrl = `${config.app.baseUrl}`;
        let queryString = app.buildQuery();
        let url = baseUrl + '?q=' + queryString;
        
        const response = await axios.get(url, { httpsAgent: agent });
              
        console.log('Fetch finished');

        return response.data;
    } catch (error) {
        console.log(error);
    }
};

app.init().catch();

module.exports = app;